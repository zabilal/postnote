package nova.postit.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import nova.postit.entity.Note;

public class DataClass {

	
	private EntityManager em = null;
    private EntityManagerFactory emf = null;
    
    //Initializer
    public DataClass(){
        connectDatabase();
    }
    
    //Database Connection
    private void connectDatabase(){
        if(emf == null){
            emf = Persistence.createEntityManagerFactory("$db/post-note.odb");
            System.out.println("Database Connected");
        }
        if(em == null){
            em = emf.createEntityManager();
        }
    }
    
    public void discon(){
    	if(em != null){
    		em.close();
    	}
    	if(emf != null){
    		emf.close();
    	}
    }
    
    //Note creation 
    public boolean addNote(String note){
        Note u = new Note(note);
        em.getTransaction().begin();
        em.persist(u);
        em.getTransaction().commit();
        return true;
    }
    
    //Pulling all Created Notes
    public List getNotes(){
        try{
            TypedQuery q = em.createQuery("SELECT p FROM Note p", Note.class);
            return q.getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    //Updating a selected Note   
    public boolean updateNote(String note, int id){
            
        Note notes = em.find(Note.class, id);
        em.getTransaction().begin();
        notes.setNote(note);
        em.getTransaction().commit();
        
        return true;
    
    }
 
    
    //Deleting a Note into the wastebin
    public boolean deleteNote(int id){
       
    	Note notes = em.find(Note.class, id);
        em.getTransaction().begin();
        em.remove(notes);
        em.getTransaction().commit();
        
        return true;
    }
}
