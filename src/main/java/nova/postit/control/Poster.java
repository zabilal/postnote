package nova.postit.control;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nova.postit.dao.DataClass;
import nova.postit.entity.Note;
import nova.postit.util.LogMan;

public class Poster {

	private DataClass data;//use context
	private LogMan log;//use context
	public static String id;//use context
	
	
	
	public static String getId() {
		return id;
	}

	public static void setId(String id) {
		Poster.id = id;
	}
	
	

	public Poster(){
		log = new LogMan();
	}
	
	public void createNote(String note){
		
		if(data == null)data = new DataClass();
		
		boolean state = data.addNote(note);
		if(state){
			log.doLog("A gentle note has been created with Title : " + note);
			data.discon();
		}else{
			log.doLog("The note is very sturbbon and failed to save");
			data.discon();
		}
		
	}
	
	public Map<Long, String> pullNotes(){
		
		//ArrayList<String> notes = new ArrayList<>();
		Map<Long, String> notes = new HashMap<>();
		if(data == null)data = new DataClass();
		
		@SuppressWarnings("unchecked")
		List<Note> param = data.getNotes();
        for(Note m : param){
          notes.put( m.getId(), m.getNote());
        }
        
        return notes;
	}
	
	public void updateNote(String note){
		
		if(data == null)data = new DataClass();
		
		boolean state = data.updateNote(note, Integer.parseInt(id));
		if(state){
			log.doLog("A gentle note has been created with Title : " + note);
			data.discon();
		}else{
			log.doLog("The note is very sturbbon and failed to save");
			data.discon();
		}
		
	}
	
	public void deleteNote(){
		
		if(data == null)data = new DataClass();
		
		boolean state = data.deleteNote(Integer.parseInt(id));
		if(state){
			log.doLog("A gentle note has been Deleted ");
			data.discon();
		}else{
			log.doLog("The note is very sturbbon and failed to Delete");
			data.discon();
		}
		
	}
}
