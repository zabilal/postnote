package nova.postit.util;

import java.util.Collection;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid.SelectionModel;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;


public class Notes extends Window implements View{
	
	private boolean editMode;
		
	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public Notes(boolean edit){
		editMode = edit;
		setModal(true);
		setDraggable(true);
		setContent(buildNote());
	}
	
	private VerticalLayout buildNote(){
		
		VerticalLayout val = new VerticalLayout();
		
		TextField title = new TextField("Add Title");
		TextArea notes = new TextArea("Write Note"); 
		
		Button save = new Button("Post it");
		save.setIcon(FontAwesome.SAVE);
    	save.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	
    	val.addComponent(title);
    	val.addComponent(notes);
    	val.addComponent(save);
    	
		return val;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

	
}
	
