package nova.postit.util;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LogMan {

	private Logger logger = Logger.getLogger("Post-Log");  
    private FileHandler fh;
    private File file;

    public void doLog(String log){
    	
    	try {  

            // This block configure the logger with handler and formatter  
    		
    		File file = new File(System.getProperty("user.dir")+"/PostNote/PostLog.log");
    		
    		if(file.exists()){
    			fh = new FileHandler(file.getAbsolutePath(), true);  
                logger.addHandler(fh);
                SimpleFormatter formatter = new SimpleFormatter();  
                fh.setFormatter(formatter);  

                // the following statement is used to log any messages  
                logger.info(log);  

    		}else{
    			if (file.getParentFile().mkdir()) {
        		    file.createNewFile();
        		} else {
        		    throw new IOException("Failed to create directory " + file.getParent());
        		}
    			
    			fh = new FileHandler(file.getAbsolutePath(), true);  
                logger.addHandler(fh);
                SimpleFormatter formatter = new SimpleFormatter();  
                fh.setFormatter(formatter);  

                // the following statement is used to log any messages  
                logger.info(log); 
    		}
            
        } catch (SecurityException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  

    }
     
}
