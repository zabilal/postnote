package nova.postit.view;

import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class Editor extends CssLayout{
	
	private VerticalLayout lay;
	private Label titleLabel;
	private TextField title;
	private TextArea note;
	private Button save;
	private Button cancel;
	private Button delete;
	
	public Editor(){
		
		addComponent(buildEditor());
		setStyleName("crud-editor");
		addStyleName("note-form");
		addStyleName("note-form-visible");
	}

	public VerticalLayout buildEditor(){
		
		lay = new VerticalLayout();
				
		title = new TextField("Add Title");
		note = new TextArea("Write Notes");
		save = new Button("Save");
		cancel = new Button("Cancel");
		delete = new Button("Delete");
		
		
		lay.addComponent(title);
		lay.addComponent(note);
		lay.addComponent(save);
		lay.addComponent(cancel);
		lay.addComponent(delete);
		
		return lay;
	}
}
