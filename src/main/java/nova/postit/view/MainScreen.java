package nova.postit.view;




import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.themes.ValoTheme;

import nova.postit.PostNoteUI;
import nova.postit.control.Poster;


/**
 * Main Content of the UI.
 * 
 * 
 */
public class MainScreen extends VerticalLayout implements View {

	
	private VerticalLayout lay;
	private Label titleLabel;
	private Button save;
	private Button cancel;
	private Button delete;
	private Poster post;
	
	private static boolean editMode;
	public static Component editor;
	private static TextArea note;
	public static VerticalLayout viewContainer;

    public MainScreen(PostNoteUI ui) {

    	Responsive.makeResponsive(this);
    	viewContainer = this;
    	setStyleName("login-screen");
    	setSpacing(true);
    	
    	post = new Poster();
    	editor = buildEditor();
    	        
        addComponent(buildTitle(ui));
        addComponent(new PostBoard());
    }
    
    private HorizontalLayout buildTitle(UI win){
    	
    	HorizontalLayout title = new HorizontalLayout();
    	title.setResponsive(true);
    	title.setWidth("100%");
    	title.setHeight("100px");
    	title.setStyleName("post-title");
    	
    	HorizontalLayout con = new HorizontalLayout();
    
    	
    	Label titleLabel = new Label("<h1><b>POST-IT NOTE MANAGEMENT TOOL</b></h1>", ContentMode.HTML);
    	titleLabel.setStyleName("label-title");
    	
    	con.addComponent(titleLabel);
    	
    	HorizontalLayout con2 = new HorizontalLayout();
    	
    	
    	Button note = new Button("New Note");
    	note.setIcon(FontAwesome.PENCIL_SQUARE);
    	note.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	
    	
    	note.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	
            	setEditMode(false);
            	editor.setStyleName("crud-editor");
            	viewContainer.addComponent(editor);
            }
        });
    	
    	con2.addComponent(note);
    	    	
    	Label logo = new Label();
    	logo.setIcon(new ThemeResource("icon.jpg"));
    	
    	title.addComponent(logo);
    	title.setComponentAlignment(logo, Alignment.MIDDLE_LEFT);
    	title.addComponent(con);
    	title.setComponentAlignment(con, Alignment.MIDDLE_CENTER);
    	title.addComponent(con2);    	
    	title.setComponentAlignment(con2, Alignment.MIDDLE_RIGHT);
    	
    	
    	return title;
    }
    
    public CssLayout buildEditor() {
        CssLayout loginInformation = new CssLayout();
        loginInformation.setStyleName("crud-editor");
       
        Label loginInfoText = new Label(
                "<h1><b>Usage Instructions</b></h1>"
                        + "On the Main Page you have all the created Notes Listed in Post Note Style, "
                        + "Create a Note Below and Clicking on Save Button to Save your Note. "
                        + "Select on any note to edit and Delete, Click on Cancel to Hide this View",
                ContentMode.HTML);
        loginInformation.addComponent(loginInfoText);
        
        lay = new VerticalLayout();
		lay.setSpacing(true);
		lay.setMargin(true);
		
		note = new TextArea("Write Notes");
		note.setWidth("100%");
		
		save = new Button("Save");
		save.setWidth("100%");
		save.setStyleName(ValoTheme.BUTTON_FRIENDLY);
		
		
		
		save.addClickListener(new ClickListener() {
            @SuppressWarnings("static-access")
			@Override
            public void buttonClick(ClickEvent event) {
            	//Save Note
            	String n = note.getValue();
            	if(isEditMode()){
            		post.updateNote(n);
                	Notification.show("You just Updated a Note", Type.HUMANIZED_MESSAGE);
            	}else{
            		post.createNote(n);
                	Notification.show("You just created a Note", Type.HUMANIZED_MESSAGE);
            	}
            	
            }
        });
		
		cancel = new Button("Cancel");
		cancel.setWidth("100%");
		
		cancel.addClickListener(new ClickListener() {
            
			@Override
            public void buttonClick(ClickEvent event) {
            	editor.removeStyleName("crud-editor");
            	editor.setStyleName("editor-out");
            	viewContainer.removeComponent(editor);
            }
        });
		
		delete = new Button("Delete");
		delete.setWidth("100%");
		delete.setStyleName(ValoTheme.BUTTON_DANGER);
		
		delete.addClickListener(new ClickListener() {
           
			@Override
            public void buttonClick(ClickEvent event) {
            	//Delete Note
				post.deleteNote();
            	Notification.show("You just Deleted a Note", Type.HUMANIZED_MESSAGE);
            	viewContainer.markAsDirty();
            }
        });
		
		
		lay.addComponent(note);
		lay.addComponent(save);
		lay.addComponent(cancel);
		lay.addComponent(delete);
		
		loginInformation.addComponent(lay);
		
        return loginInformation;
    }
    

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

	public static TextArea getNote() {
		return note;
	}

	public void setNote(TextArea note) {
		this.note = note;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public static void setEditMode(boolean editMode) {
		MainScreen.editMode = editMode;
	}

	
}
