package nova.postit.view;

import java.util.ArrayList;
import java.util.Map;

import com.vaadin.event.LayoutEvents;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.server.Page;
import com.vaadin.server.Page.Styles;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

import nova.postit.control.Poster;

public class PostBoard extends VerticalLayout{

	private int rows;
	private int cols;
	private Panel note;
	private Label content;
	private Poster post;//use context
	public static String id;//use context
	
	public PostBoard(){
		
		post = new Poster();
		
		addComponent(buildNote());
	}
	
	@SuppressWarnings("unchecked")
	protected GridLayout buildNote(){
		
//		ArrayList<String> notes = post.pullNotes();
		Map<Long, String> notes = post.pullNotes();
		
		int len = notes.size();
		
		if(len < 5){
			cols = len;
			rows = 1;
		}else{
			cols = 5;
			rows = (len/cols) + 1;
		}
		
		GridLayout grid = new GridLayout(cols, rows);
		grid.setResponsive(true);
		grid.setSizeFull();
		grid.setSpacing(true);
		
		for (Map.Entry<Long, String> entry : notes.entrySet()) {
			
			content = new Label(entry.getValue());
			
			content.setStyleName("v-panel-content");
					
			note = new Panel(content);
			note.setStyleName("panel-anime");
		
			grid.addComponent(note);
			content.setId(String.valueOf(entry.getKey()));
		}

		
		grid.addLayoutClickListener(new LayoutClickListener() {
			public void layoutClick(LayoutClickEvent event) {
				if(event.getClickedComponent() != null){
					Label label = (Label)event.getClickedComponent();
					String message = label.getValue();
					Poster.setId(event.getClickedComponent().getId());
					System.out.println(event.getClickedComponent().getId());
					MainScreen.setEditMode(true);
					MainScreen.getNote().setValue(message);
					MainScreen.editor.setStyleName("crud-editor");
					MainScreen.viewContainer.addComponent(MainScreen.editor);
				}
			}
		});
		
		return grid;
	}
}
