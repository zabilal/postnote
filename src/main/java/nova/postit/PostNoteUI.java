package nova.postit;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import nova.postit.view.MainScreen;


/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
@Widgetset("nova.postit.note.MyAppWidgetset")
public class PostNoteUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
    	Responsive.makeResponsive(this);
        setLocale(vaadinRequest.getLocale());
        getPage().setTitle("Post Note Manager");
        addStyleName(ValoTheme.UI_WITH_MENU);
        
       	Navigator nav = new Navigator(this, this);
        nav.addView("", new MainScreen(PostNoteUI.this));
        
        
    }
    
    public static PostNoteUI get() {
        return (PostNoteUI) UI.getCurrent();
    }
   

    @WebServlet(urlPatterns = "/*", name = "PostNoteUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = PostNoteUI.class, productionMode = false)
    public static class PostNoteUIServlet extends VaadinServlet {
    }
}
